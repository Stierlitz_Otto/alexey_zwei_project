import "./index.scss";

export const NotFound = () => {
	return (
		<div className="NotFound">
			<span className="NotFound__error">404 Page not Found</span>
		</div>
	);
};
